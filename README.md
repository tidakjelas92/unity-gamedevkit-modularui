# unity-gamedevkit-modularui

Modular UI module for generic UI creation.

## Installation Guide

Package Manager dependencies:
- GamedevKit (com.umnp.gamedevkit)
- GamedevKit Tweeners (com.umnp.gamedevkit.tweeners)

Asset Store dependencies:

- DOTween (HOTween v2) by Demigiant
  > 1. Import from [Asset Store](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676)
  > 2. Setup .asmdef for DOTween using Tools > Demigiant > DOTween Utility Panel > Create ASMDEF. Make sure to include all modules.

### Remarks

The asmdef references DOTween.Modules.asmdef using name instead of GUID. Changing the name (although very unlikely) will break the dependency.
