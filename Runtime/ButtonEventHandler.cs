using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UMNP.GamedevKit
{
    [RequireComponent(typeof(Button))]
    public class ButtonEventHandler : MonoBehaviour
    {
        [SerializeField] private UnityEvent<BaseEventData> _onPointerClick;

        private Button _button;

        private void Awake() => _button = GetComponent<Button>();

        public void OnPointerClick(BaseEventData eventData)
        {
            if (!_button.interactable) return;
            _onPointerClick?.Invoke(eventData);
        }
    }
}
