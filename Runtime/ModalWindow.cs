using System;
using DG.Tweening;
using UMNP.GamedevKit.Pooling;
using UMNP.GamedevKit.Tweeners;
using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.ModularUI
{
    [RequireComponent(typeof(FadeTweener))]
    public abstract class ModalWindow : PoolObject
    {
        [Header("Window References")]
        [SerializeField] private PositionTweener _positionTweener;
        [SerializeField] private Selectable[] _controls = new Selectable[] { };

        [Header("Window Parameters")]
        [SerializeField] private OffsetTweenKey _showTweenKey = new OffsetTweenKey(new TweenKey(0.4f, Ease.OutExpo), new Vector2(0.0f, -200.0f));
        [SerializeField] private OffsetTweenKey _hideTweenKey = new OffsetTweenKey(new TweenKey(0.3f, Ease.InExpo), new Vector2(0.0f, 200.0f));

        private FadeTweener _fadeTweener;
        private Action _onHideComplete;
        private Action _onHideStart;
        private Action _onShowComplete;
        private Action _onShowStart;

        protected virtual void Awake() => _fadeTweener = GetComponent<FadeTweener>();

        public void Show()
        {
            if (_fadeTweener.IsPlaying()) return;

            _fadeTweener.FadeIn(
                _showTweenKey.tweenKey,
                () =>
                {
                    SetControlsInteractable(false);
                    _onShowStart?.Invoke();
                },
                () =>
                {
                    SetControlsInteractable(true);
                    _onShowComplete?.Invoke();
                }
            );
            _positionTweener.MoveTo(_showTweenKey.offset, _showTweenKey.tweenKey);
        }

        public void Hide()
        {
            if (_fadeTweener.IsPlaying()) return;

            _fadeTweener.FadeOut(
                _hideTweenKey.tweenKey,
                () =>
                {
                    SetControlsInteractable(false);
                    _onHideStart?.Invoke();
                },
                () =>
                {
                    SetControlsInteractable(true);
                    _onHideComplete?.Invoke();
                }
            );
            _positionTweener.MoveFrom(_hideTweenKey.offset, _hideTweenKey.tweenKey, null, Despawn);
        }

        /// <summary>Setup a one time use callback to the end of hide animation</summary>
        public void SetOnHideComplete(in Action callback) => _onHideComplete = callback;
        /// <summary>Setup a one time use callback to the start of hide animation</summary>
        public void SetOnHideStart(in Action callback) => _onHideStart = callback;
        /// <summary>Setup a one time use callback to the end of show animation</summary>
        public void SetOnShowComplete(in Action callback) => _onShowComplete = callback;
        /// <summary>Setup a one time use callback to the start of show animation</summary>
        public void SetOnShowStart(in Action callback) => _onShowStart = callback;

        protected override void OnDespawn()
        {
            _onHideComplete = null;
            _onHideStart = null;
            _onShowComplete = null;
            _onShowStart = null;
        }

        private void SetControlsInteractable(bool interactable)
        {
            foreach (var control in _controls)
                control.interactable = interactable;
        }
    }
}
